//a bare-bones website example
// npm install && node web.js

const cmd = require('node-cmd');
const express = require('express');
const app = express( );
const internalIp = require('internal-ip');

app.get('/', function(req, res, next){
    res.send('Hello World from lenovo-ubuntu');
});

const PORT = 3000;
app.listen(PORT, function( ){
    console.log(`quick start app listening on port ${ PORT}`);
    
    // determine our internal ip (for accessing this site via local network)
    console.log('internal ip = ', internalIp.v4.sync())
    // console.log(internalIp.v6.sync())

    // cmd.get('ifconfig | grep Bcast', function(err, data, stdErr){
    //     const start = data.indexOf('inet') + 10;
    //     const end =  data.indexOf('Bcast');
    //     console.log(`internal ip = ${ data.substr(start, end - start)}`);
    // });

    // use an external website to determine our external ip
    cmd.get('curl ipinfo.io/ip', function(err, data, stdErr){
        console.log(`external ip = ${ data}`);
    });
});
