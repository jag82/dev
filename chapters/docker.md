# Docker

Running code in Docker containers allows you to make sure all dependenices are reliably installed without manual steps across most platforms.

# Basic Commands


## List
Show all local docker images.
`docker image ls`  
`docker images -a`  

Show all(?) containers.
`docker container ls`  
TODO: `docker ps` 


## Remove
TODO: `docker system prune`  

Remove a docker image.
`docker rmi Image Image`  

Remove all docker images.
`docker rmi $(docker images -a -q)`  


Search remote docker images.
`docker search <term>`  

Delete a container.
`docker rm <container>`  

Delete all stopped containers.
`docker rm $(docker ps -a -q)`

TODO: volumes

https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes

# Single Process

1. Create a `Dockerfile` (see `_dev/Dockerfile.sample`).  

```
FROM node:10

WORKDIR ~/app

COPY package*.json ./

RUN npm install

# by copying the rest of the files in a separate step, we don't redo dependency installations when our source code changes
COPY . .

# this command isn't run when building a docker image, but is the entrypoint for running a docker container based on the image
CMD npm run start
```

2. Create an app (e.g. a simple nodejs application).  

3. Run a docker container.

`docker build -t <tag> .`  

`docker images`  

`docker run -d -p 80:4000 <tag>`  

4. Visit your site @ `localhost:80`.

https://nodejs.org/de/docs/guides/nodejs-docker-webapp/


# Multi Process

You want to run an app and a database together? For this, and other complex cases, we will create a `docker-compose.yml`.

* Yaml is a superset of json so any JSON file should be valid YAML.*

```

```

https://docs.docker.com/compose/
$ docker-compose up -d
$ ./run_tests
$ docker-compose down



https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
