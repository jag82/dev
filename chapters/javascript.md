# Javascript

## Vanilla javascript

## ES6
https://github.com/lukehoban/es6features#readme

## Helper Libraries
jQuery

## Async Code
callbacks
promises    https://developers.google.com/web/fundamentals/primers/promises
async/await

### Promises
https://codeburst.io/a-simple-guide-to-es6-promises-d71bacd2e13a