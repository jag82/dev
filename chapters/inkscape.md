# inkscape

Inkscape is a free program to make SVGs (scalabale vector graphics).  


## bread and butter shortcuts

`S` = select tool
`B` = bezier/pen tool
`N` = edit nodes tool


`1` = zoom 1:1
`2` = zoom 1:2
`3` = zoom to selection
`4` = zoom to drawing
`5` = zoom to page
`cmd + 4` = center

`cmd + D` = duplicate
`option + D` = clone (linked to original)
`option + shift + D` = unlink clone
`shift + D` = lookup original

`cmd + G` = group
`cmd + shift + G` = ungroup

`cmd + shift + L` = layers
`cmd + shift + F` = fill/stroke
`cmd + shift + A` = alignment
`cmd + shift + T` = text
`cmd + shift + H` = (custom: Toggle All Dialogs)

`cmd + [` = rotate 90 degrees
`[` = rotate 15 degrees

`ctrl + shift + 7` = Path Effects (e.g. choose Perspective/Envelope, use the Node tool)

`cmd + I` = import image
`cmd + shift + E` = export to image

`cmd + ctrl + A` = select all in current layer
`cmd + 6` = split view
`option + 6` = x-ray view

`cmd + shift + D` = document properties
`cmd + shift + P` = open preferences (unusual for mac)


To change shortcuts:  
`Preferences > Interface > Keyboard`


## adding a custom color palette

Create a text file with the extension `.gpl` at `Applications > Inkscape > (Show Package Contents) > Contents > Resources > share > inkscape > palettes` with contents like:  
```
MyColorPalette
ffffff
aa00aa
```
