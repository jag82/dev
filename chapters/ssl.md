# ssl
In order to have an `https://` address instead of an `http://` (which will give users all kinds of scary warnings and make them susceptible to attacks), you'll need to set up SSL.  

An easy, free way to do it is to use Let's Encrypt and Certbot.  Note that I'm running Certbot based on the host (digital ocean) and not the domain provider (gandi).  

https://letsencrypt.org/  
https://certbot.eff.org/  

Here's an example that worked for `trashtelier.de` (domain provided by Gandi, hosting by Digital Ocean).  
https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04  

```
sudo apt install certbot python3-certbot-nginx

sudo ufw allow "Nginx Full"

sudo certbot --nginx -d trashtelier.de -d www.trashtelier.de
```

Let's Encrypt certificates are only valid for 90 days. This is to encourage automatic renewals, which is already set up for us by certbot.

Check the cronjob via:
```
sudo systemctl status certbot.timer
```

Confirm renewal will work with a dry run:
```
sudo certbot renew --dry-run
```

## TODO:
What can go wrong? A.k.a. what should i backup and where?  
How to set up wildcard domains (*.trashtelier.de)?


sudo certbot --nginx -d trashtelier.de -d www.trashtelier.de -d ideas.trashtelier.de -d karaoke.trashtelier.de
