# Arduino

See `arduino-samples` repo.

Arduino has its own IDE, but it's trash. You're much better off taking your editor of choice (Sublime Text?) and using it to control the Arduino IDE.

There are several plugins: Stino, Deviot, and arduino-cli. Only Deviot seems to have been recently updated. I've never been happy with any of them.

It's easier to open your sketch in the Arduino IDE > Preferences > Use External Editor. This will grey out the Arduino IDE. You can now open up the same file in another editor (Sublime Text) and work from there. To build/run, return to Arduino IDE.

For syntax highlighting:  
View > Syntax > Open all with current extension as... > C++.

For error highlighting:  
- install SublimeLinter via PackageManager
- install SublimeLinter-cpplint
- install cpplint via commandline: `pip install cpplint`

Note that it will work on `.cpp` files. To get it to run on `.ino` files: 
Sublime Text > Preferences > Package Settings > SublimeLinter >
```
    "linters": {
        "cpplint": {
            "args": ["--extensions=ino"]
        }
    }
```

You may want to also setup `eslint_d` if linting is slow on your system.  
