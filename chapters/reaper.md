# reaper

TODO:
cutting up loops
https://youtu.be/rb3L8HjTtbc?si=eb-jFBXX4uwju-DX

## record audio from an external microphone (e.g. USB microphone)

1. Plug the microphone in.

2. Go to `Settings > Audio > Device`

3. Set `Input Device` to your microphone (e.g. TIE Condenser USB Microphone).

4. Select `allow use of different input/output devices` and choose `External headphones`. 

5. Create a new Track. Make sure it's armed for recording (red dot with an O). Set the input to `Input: Stereo > Front Left / Front Right`.

6. Record via `cmd + R` or the larger record button (red dot with an O) below the Tracks section.


## real time voice/input modification

1. Set up and audio recording Track (see above).

2. Add a Plugin by clicking FX on the Track.  Add a plugin (e.g. ReaVerb) and configure it (e.g. `Add > Echo Generator`, `Length > 500ms`, `Spacing 150ms`).

3. Whenever you hit the "power" button next to FX, you will have this effect applied to the output device.  
