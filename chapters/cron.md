# cron
Cronjobs are instructions for running scripts on a regular schedule.  

They live in the following files/directories:  
`/etc/crontab`  
`/etc/cron.*/`  
`/var/spool/cron/`  

https://www.cyberciti.biz/faq/how-do-i-add-jobs-to-cron-under-linux-or-unix-oses/  


*add a cronjob*  
`crontab -e`  

*list cronjobs*  
`crontab -l`  
`crontab -u <username> -l`  

*remove all cronjobs*
`crontab -r`  
