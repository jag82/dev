# images

processing images can be useful (e.g. emoto-booth, a photo booth project that does some image post processing to remove background, make black/white photos, etc.)  

Here are some useful shell scripts:  

```
toBw() {
   if [[ $# -lt 1 ]]; then
      echo "Usage: toBw <image-file> [% black]"
      return 1
   fi
   
   bw="${2:-50}"
  if ! [[ "$bw" =~ ^[0-9]+$ ]]; then
    echo "Error: Second argument must be an integer."
    return 1
  fi

   input="$1"  # some-image.png
   output="${input%.*}.bw${bw}.${input##*.}" # some-image.bw50.png
  
   magick ${input} -colorspace gray -threshold ${bw}% -background black -alpha remove ${output}
   echo "Converted $input to $output"
}

```