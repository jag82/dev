# Regex

Regexes allow you to perform match/replace text. They are powerful but difficult to read.

TODO regex resources

TODO important regex examples in js

.test returns true/false

.exec

// ^ = starts with,  $ = ends with
/^[A-Za-z0-9]$/.test('some string')

// ? = 0 or 1 time, same as {0,1}
/^[A-Za-z0-9]?$/

// + = 1 or more time, same as {1,}
/^[A-Za-z0-9]+$/

// * = any number of times, same as {0,}
/^[A-Za-z0-9]*$/

// ^ inside square brackets is 'not matching'
/^[^A-Za-z0-9]+$/



[Regex Checker](https://www.regexpal.com/)
