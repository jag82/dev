# blender

[Learn Blender in 2024](https://youtu.be/iCmaM7oobUY?si=F-AtPOe7IbAE5RML)  

[How to make floor plans](https://www.youtube.com/watch?v=Q4rbqUbhYXY) (hint: use archimesh)


TODO: 
[adding photo textures to faces/objects](https://youtu.be/yQMsN0J4tOM?si=OsdZROS65AQzGVpR)
The Bricker plugin will lego-fy your 3d objects?
exploring models in vr?


## shortcuts

cmd + space
	, M 			measure tool
	, other letters other tools

`CMD + , > Input > Emulate Numpad`


TODO: parenting (ctrl+P), inserting keyframes(I)

https://www.youtube.com/watch?v=MJfQ4dMKNL4

Right click anything in blender > add to Quick Favorites
Will show up in Q menu


**navigate**
(Important for laptop keyboards: use the ` key to switch views like top, left, camera, etc.)

Camera + 

` 					pie menu for views listed below
	+ shift 		camera fly mode (mouse + WASD)

2-finger-mouse 		orbit around 3d-cursor
	+ shift			pan
	+ cmd			zoom

H 					hide
	+ shift			hide all but selected
	+ alt			reveal

Z   				switch view mode (e.g. solid, wireframe)

1 					view along Y axis
	+ ctrl 			from the back
2 					spin on Y

3 					view along X axis
	+ ctrl 			from the back
4 					spin on X

7					view along Z axis
	+ ctrl 			from the back
8 					spin on Z

5
6
9 					flip view 180 degrees
0   


**select**
A 					select all
	, A				select none
I
	+ ctrl 			invert selection

L 					select everything linked to mesh under cursor
	+ ctrl 			link selected objects

G					
	+ shift			select similar parts of mesh

S
	+ shift 		(cursor to selected, helps with making new object where you want them)

LMB 				select
	+ alt			select through object (or select edge loop in edit mode?)

shift + C 			reset 3d cursor position
N 					TODO: does some things with 3d cursor

**basic operations**
(alt + R, G, S = clear rotation, position, scale)7
(ctrl + R) = subdivide

A
	+ shift			add object

G 					translate
S 					scale
	+ shift			pie menu: set cursor!
R 					rotate
	+ alt			clear
	, XX/YY/ZZ		use global/local x/y/z axis for translate/scale/rotate
	+ ctrl 			add edge loops (use mouse wheel to add more)

I 					inset
E 					extrude
	, XX/YY/ZZ 		along axis
	, B 			with basepoint
	+ alt			extrude with options

B
	+ ctrl 			bevel (mouse wheel to adjust)

D
	+ shift			duplicates object
	+ alt 			duplicates and links objects (in edit mode they are linked, not object mode)

M
	+ ctrl, X/Y/Z 	mirror along X/Y/Z axis

X 					delete



shift + R 			repeat last action
shift + S 			pie menu to replace 3d cursor

**edit mode details**
V 	+ cmd			vertex menu
E 	+ cmd			edge menu
F   + cmd			face menu

G 	+ shift			select similar

1,2,3 				vertex/edge/face mode (if in edit mode)

**interface**
T 					toggle toolbar (left)
N 					toggle sidebar (right)
TAB 				toggle between object/edit mode

**document**



ctrl + space		maximize window
ctrl + alt + space	fullscreen
ctrl + alt + Q		toggle quad view


ctrl +/-			increase/decrease selection

shift + O 			switch between proportional editing options

shift + num1,3,7	align view along normals of selected item

ctrl + B 			bevel edges
ctril + shift + B 	bevel vertices
ctrl + B 			crop the render (useful for previews, ctrl + alt + B to reset)
ctrl + 0			make selected object the active camera (useful for placing lights)


ctrl + S 			save
	+ alt			save with iterated name (mysave1.blend)
ctrl + F2 			batch rename