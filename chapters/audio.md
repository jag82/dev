# audio

For simple audio playback, editing, and programmatic control first consider the tools `sox` and `aplay`(?). `mpv` is also useful for playback (see video section).

https://linux.die.net/man/1/sox  
https://formulae.brew.sh/formula/sox  


## mpv  
Play video files from the commandline, but also pure audio files?

## sox
`apt-get install sox`  

`sox TODO`  

`soxi TODO`  

For simple audio cuts and conversion, consider `ffmpeg` (see video chapter).  




What about aplay, afplay?

What about alsamixer, jack, pulseaudio?  

What about cadence, carla, etc?  


## recording computer audio (and video)
Sometimes the easiest thing to do is record the audio output of your computer (e.g. when streaming).  Here's how to do it on a mac:  

Use OBS Studio and simply record your screen (e.g. Mac OS Screen Capture).  To adjust the recording volume, regular audio controls won't work. Instead click `Audio Mixer > Filters` and add a Gain filter. You may also want to add a Compressor filter to prevent loud spikes.  Under `Audio Mixer > ... > Advanced Audio Properties` you can activate Monitoring to hear the end result in your headphones.  

You can use `ffmpeg` to convert the resulting video into an audio file from the commandline.  
`ffmpeg -i input.mkv -vn -c:a copy output.mp4`  
(vn = no video, c:a = audio codec, in this case we copy without re-encoding)  

A more complex example:
`ffmpeg -i input.mkv -ss 00:00:57.550 -to 00:02:07.000 -vn -c:a copy output.mp4`


For convenience, I recommand adding this to your shell shorcuts (e.g. `.zshrc`):  
```
toAudio() {
   if [[ $# -lt 1 ]]; then
      echo "Usage: toAudio <some-video.mkv>"
      return 1
   fi

   input="$1"  # some-video.mkv
   output="${input%.*}.mp3" # some-video.mp3
  
   ffmpeg -i ${input} -vn -c:a libmp3lame -q:a 2 ${output}
   echo "Converted $input to $output"
}
```



### deprecated: blackhole and soundflower

Old solutions reference SoundFlower, but I believe this is rotting.  A newer solution is to use BlackHole: https://github.com/ExistentialAudio/BlackHole.  BlackHole is a virtual audio driver that allows you to route audio to programs with zero latency. Here are some instructions: https://www.youtube.com/watch?v=sT7YKMTdnX4  

1. Install BlackHole, set it up as per its instructions.  

2. Go to Audio Midi Settings and setup a Multi-Output Device (for playing sound to BlackHole at the same time as our Internal Speakers).  For completness, you can do the same for an Aggregate Device (for recording).  

You should now be able to play audio, record it in audacity, and hear it play out of your speakers (so you can hear what you're recording).  

Useful audacity shorcuts (J + K to jump cursor to start/end, cmd+I to split track at selection marker, cmd+F to fit to screen, ctrl+E selection fills screen). 

(To export split clips within a single track: select a clip > cmd+B creates a label > alt+. goes to the next clip > repeat > cmd+shift+L will export multiple) -- this doesn't work the way I want it to, just select a clip and cmd+shift+E for now.


### deprecated: Reaper
Improvment: record in Reaper instead!  

1. Reaper > Options > Preferences (cmd+,) > Audio > Device > Input Device: **Blackhole 2ch**  

2. New Track (cmd+T), arm the track for recording (click the red O-button on the track)

3. Start recording (cmd+R, or click the red O-button below all the tracks)  

WARNING: there's a slight echo -- can this be fixed with drift correction?

