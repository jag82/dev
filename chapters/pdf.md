# pdf

PDFs can be tricky to work with. Here are easy solutions to common tasks.  


## add a signature to a pdf
Open in Preview on a mac > Click the icon that looks like a circle with a marker in it > Click the "Sign" drop-down that has a picture of signature on it

## turn images into a single pdf
Right click on a mac > Quick Actions > Create PDF
