# Network (and Ports?)

Router addresses typically look like `http://192.168.178.1/` and can be accessed in a browser connected to the local network.

## Reserved Ports (TODO clean this up!)

Port 80 is the default http port and you'll likely run into errors trying to host a website on it. Here's one way to use it (not ideal):

https://www.digitalocean.com/community/questions/how-can-i-get-node-js-to-listen-on-port-80  

`sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 3000`  

to reset your iptables to defaults  

`sudo iptables -F`  

also useful:
`ifconfig -a | grep inet`

setting up a the firewall to allow ports:
https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-14-04

`sudo ufw allow http`  
`sudo ufw allow ssh`  

Or just use nginx (ideal):
https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04-quickstart

https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-18-04

## Static Internal Address (TODO perhaps it's better to set the hostname and scan for the suffix (e.g. .local, .fritz.box))

Reserve an DCHP IP by binding it to your MAC address.
https://www.howtogeek.com/177648/how-to-force-your-pc-to-keep-its-private-ip-address/



### Getting
```
BROADCAST_LINE=`ifconfig | grep inet.*Bcast`
IP_INTERNAL=`expr match "$BROADCAST_LINE" '.*inet addr:\(.*\)Bcast'`
echo "$IP_INTERNAL"
```

Address Resolution Protocol
```
arp -a
```


### Setting
Edit `/etc/dhcpcd.conf` by adding the following lines. You'll need your router's IP address (TODO how to get from commandline)

**WARNING** note the 192.168.178.1 as the router's IP in the example below. It is used for both routers and domain_name_servers. The first 3 segments are also used in the chosen static ip_address.

*Ethernet*
```
interface eth0

static ip_address=192.168.178.10/24
static routers=192.168.178.1
static domain_name_servers=192.168.178.1
```

*WLAN*
```
interface wlan0

static ip_address=192.168.178.200/24
static routers=192.168.178.1
static domain_name_servers=192.168.178.1
```

Then `sudo reboot` and confirm the changes with `ifconfig`.


##Static External Address

### Getting

```
curl ipinfo.io/ip
```

### Setting

Normally you'll have to pay for an static external IP, so we're likely to use the following workaround.

1. Purchase a domain name.

2. Use DDNS (Dynamic Domain Name Server) to periodically check the server's external IP and compare it to the **zone file** with the domain registrar. [This tool is well suited to `gandi.net` and a raspberry pi](https://github.com/matt1/gandi-ddns).

3. Setup *port forwarding* in your router. The address is typically something like `192.168.178.1`. TODO forward ports 22 for SSH, forward 80 for http and 443 for https.

