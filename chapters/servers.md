# Servers
Whatever server is set up, some basic security and dependencies are advised.


## Security
*Create a User*
Create a user with reduced priveleges rather than using `root` all the time.
```
adduser jag
```

You will undoubtedly need root priveleges at some point. Rather than logging out/in, we will add your user to the superuser group so you can type `sudo` (superuser do) before a command when you need root priveleges.
```
usermod -aG sudo jag
```

Assuming you've ssh'd into your server as root. You can copy your public keys (and all appropriate permissions) to this new user's directory.
```
rsync --archive --chown=jag:jag ~/.ssh /home/jag
```

**STAY LOGGED IN AS THE ROOT USER UNTIL YOU'VE SUCCEEDED LOGGING IN WITH YOUR NEW USER**
```
ssh jag@<your-ip>
```

https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-20-04  


*Create a Firewall*
Some applications are registered with `ufw` (unix firewall) upon installation. This allows `ufw` to manage them by name.

To see a list of available apps:
```
ufw app list
```

Allow OpenSSH through the firewall or you won't be able to log back in via ssh!
```
ufw allow OpenSSH 
```

Finally, enable the firewall:
```
ufw enable
```

Check that status of the firewall:
```
ufw status
```

You'll likely want to enable all http (port 80) and https (port 443) traffic:
```
ufw allow http
ufw allow https
```

https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands  


## Remote Servers

A simple option for a remote machine is a Digital Ocean droplet. We currently use a $5/month plan.

[How to setup a nodejs web app on Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-16-04)


## Local Servers

We can use Pis (see `raspberry-pi.md`) or our local machines as servers.

To configure the accessibility of these servers (e.g. the internet, local networks), see `network.md`.


## SSH Access
See [SSH](./ssh.md).  

Typically, you will access both remote and local servers via SSH. Here's how to allow SSH access to a linux machine.

https://www.cyberciti.biz/faq/ubuntu-linux-install-openssh-server/

Install openssh server.
```
sudo apt install openssh-server
```

Start ssh program.
```
sudo systemctl enable ssh
sudo systemctl start ssh
```

Test that it works by ssh'ing from the *same machine*.
```
hostname                # returns your computer's name on the network
ssh <user>@<hostname>
exit                    # exit ssh mode
```

Enable access through the firewall.
```
sudo ufw allow ssh
sudo ufw enable 
sudo ufw status
```

Test that it works by ssh'ing from *another machine*.
```
ssh <user>@<hostname>
```

## Nginx
Nginx is a better solution than using an nodejs reverse proxy (see below).  

Install ngnix:
```
sudo apt install ngnix
```

Adjust the firewall:
```
sudo ufw app list
sudo ufw allow 'Nginx Full'
sudo ufw status
```

After installation, ngnix is already running. Confirm this with:
```
systemctl status nginx
```

If you visit `http://<your ip>` you should see a default ngnix website.  

If not, check what's listening on port 80 (default http port) and kill it:
```
sudo lsof -i tcp:80
kill -9 <pid>
```
Then restart ngnix:
```
sudo systemctl stop nginx
sudo systemctl start nginx

sudo systemctl restart nginx
```

Setup server blocks to enable proper control of web apps:
```
sudo mkdir -p /var/www/trashtelier/html
```

Setup appropriate permissions:
```
sudo chown -R jag:jag /var/www/trashtelier/html
sudo chmod -R 755 /var/www/trashtelier
```

Add a simple html page to `/var/www/trashtelier/html`:
```
<html>
  <body>
    Test.
  </body>
</html>
```

Create a new configuration file at `/etc/nginx/sites-available` called `trashtelier`.  
```
server {
        listen 80;
        listen [::]:80;

        root /var/www/trashtelier/html;
        index index.html index.htm index.nginx-debian.html;

        server_name trashtelier.de www.trashtelier.de;

        location / {
                try_files $uri $uri/ =404;
        }
}
```

We need to symlink it for it to be picked up:
```
sudo ln -s /etc/nginx/sites-available/your_domain /etc/nginx/sites-enabled/
```

Also, to prevent an obscure error, `sudo nano /etc/nginx/nginx.conf` and uncomment `server_names_hash_bucket_size 64;`.

Finally, check for syntax errors: `sudo nginx -t` and restart ngnix `sudo systemctl restart nginx`.  

If you wanted to point different subdomains to different local apps, your `sites-available` might look like this (e.g. with two different node apps running on ports 4000 and 5000):
```
server {
    listen       80;
    server_name  trashtelier.de www.trashtelier.de;

    location / {
        proxy_pass http://127.0.0.1:4000;
    }
}
server {
    listen       80;
    server_name  test.trashtelier.de;

    location / {
        proxy_pass http://127.0.0.1:5000;
    }
}
```

To server SSL (see also SSL chapter):
```
TODO:
```

https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04  


## Reverse Proxy

If you want to host multiple sites on the same server or perform load balancing (many copies of the same site), there are two recommend options.

The simple option is to use the `reverse-proxy` npm package to make a nodejs website that listens to port 80 (the default http port) and redirects to nodejs sites running on other local ports based on their name (e.g. files.jag.tools -> localhost:3001, convert.jag.tools -> localhost: 3002).

The more robust option is to use `nginx`. https://www.tecmint.com/nginx-as-reverse-proxy-for-nodejs-app/  

Once installed, here's how a simple setup:

*create a `sysmon.conf` file*
```
sudo vi /etc/nginx/conf.d/sysmon.conf 
```


*add a server block*
```
server {
    listen 80;
    server_name sysmon.tecmint.lan;

    location / {
        proxy_set_header   X-Forwarded-For $remote_addr;
        proxy_set_header   Host $http_host;
        proxy_pass         http://192.168.43.31:5000;
    }
}
```

or
```
server {
    listen 80;

    server_name your-domain.com;

    location / {
        proxy_pass http://localhost:{YOUR_PORT};
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
```

*restart ngnix*
```
systemctl restart nginx
```

*update /etc/hosts*
```
192.168.43.31 sysmon.tecmint.lan
```

You can now visit `http://sysmon.tecmint.lan` to see the app running on port 5000.

Lookup *upstream* blocks for load balancing.

## Restarting on Server Reboots

```
crontab -e
```

```
@reboot /path/to/starter.sh
```

## Hosting / Domain Registration
See [SSL](./ssl.md).  

So far, I've used gandi.net. Here's a sample [Zonefile](https://en.wikipedia.org/wiki/Zone_file).
(the IP on the first line is the IP of your site)
```
@ 10800 IN A 217.70.184.38
blog 10800 IN CNAME blogs.vip.gandi.net.
imap 10800 IN CNAME access.mail.gandi.net.
pop 10800 IN CNAME access.mail.gandi.net.
smtp 10800 IN CNAME relay.mail.gandi.net.
webmail 10800 IN CNAME webmail.gandi.net.
www 10800 IN CNAME webredir.vip.gandi.net.
@ 10800 IN MX 50 fb.mail.gandi.net.
@ 10800 IN MX 10 spool.mail.gandi.net.
@ 10800 IN TXT "v=spf1 include:_mailcust.gandi.net ?all"

```

You might, for example, have your domain (jag.tools) registered at Gandi, but your code hosted at Digital Ocean (a simple Ubuntu instance).  In this case, you would add this line to your Zone file to redirect <anything>.jag.tools to your code hosted on Digital Ocean.
(the IP on this line is the IP of your Digital Ocean server)
```
* 10800 IN A 165.227.132.191
```

`now`
This is a way of publishing a web app on the internet quickly. Very easy to use. Good for demos, bad for production. 

Which defines the following urls:

 - TODO:
 - TODO:
 
 ## Processes

 `nodemon`
Restarts node server whenever changes are made to source files. This is good during development. 

 `forever`
 Restarts node server whenever it crashes. It also can run it in the background. This is good for production.

 ```
 npm install -g forever

 forever start index.js

 forever list

tail -f /path/to/logfile
 ```
 https://blog.nodejitsu.com/keep-a-nodejs-server-up-with-forever/

 `pm2`
 Similar to forever. Digital Ocean has tutorials on using it:
 https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-16-04

