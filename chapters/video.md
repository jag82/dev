# video

For simple video playback, editing, and programmatic control first consider the tools `ffmpeg` and `mpv`.

For advanced editing consider using Blender.


## capturing videos

** built-in tool**  
mac: `ctrl + shift + 5`  

**Good software for screen captures (saved as video or streamed):**  
Open Broadcaster Software  
https://obsproject.com/download  
`snap install obs-studio`  

## trimming videos

Trim your videos to save everyone's time and take the stress off having to have a perfectly captured video!

**LosslessCut**  
https://github.com/mifi/lossless-cut
Works great. Very simple. Good keyboard shorcuts.


## compressing/converting videos

**ffmpeg**
Great for programmatic control of video formats. Underlies most video tools.  

**Convert videos easily:**  
`ffmpeg -i some-video.mkv -o some-video.avi`  
-i = input  
-o = output  

**Compress videos:**  
`ffmpeg -i input.mp4 -vcodec libx265 -crf 28 output.mp4`  
https://unix.stackexchange.com/questions/28803/how-can-i-reduce-a-videos-size-with-ffmpeg  
-i = input
-vcodec = video codec  (libx267 corresponds to H.265)  
-crf = constant rate factor (higher = more compression)  

**To specify a new resolution:**  
`ffmpeg -i input.mp4 -vf scale=480:320 -vcodec libx265 -crf 28 output.mp4`  
https://ottverse.com/change-resolution-resize-scale-video-using-ffmpeg/#How_to_Change_the_Videos_Resolution_but_Keep_the_Aspect_Ratio  


**Inspect contents of video:**  
`ffprobe -i input.mkv`  
(this will show you the available video and audio streams and their types)


**Extracting audio from a video:**  
`ffmpeg -i input.mkv -vn -c:a copy output.mp4`  
(vn = no video, c:a = audio codec, in this case we copy without re-encoding)  

A more complex example:
`ffmpeg -i input.mkv -ss 00:00:57.550 -to 00:02:07.000 -vn -c:a copy output.mp4`



**Cutting audio:**  
From 1:00 to 2:30
`ffmpeg -i input.mp3 -ss 00:01:00 -to 00:02:30 -c copy output.mp3`

From 1:00 for 10 seconds
`ffmpeg -i input.mp3 -ss 00:01:00 -t 00:00:10 -c copy output.mp3`


princes-of-the-new-world.mp3

TODO: what about stripping/replacing audio?

TODO: what about adding/removing subtitles?



## playing videos

**mpv**
Great for playing videos and being controlled programmatically.  

Keyboard shortcuts:  
https://gist.github.com/flatlinebb/07caa79fd3b9f3770788df21756a4611
https://defkey.com/mpv-media-player-shortcuts 
https://github.com/mpv-player/mpv/blob/master/etc/input.conf  

There are a lot of handy extensions that add functionality you might want:  
https://github.com/mpv-player/mpv/wiki/User-Scripts  

I recommend creating a `~/.config/mpv/mpv.conf`  
```
osd-bar=yes
osd-duration=60000
osd-fractions=yes
```


## generate gifs
Animated gifs are great for small, short "videos".  

**giphy.com** works very well, max length of 15s.  

`gifify` is deprecated, requires `gifsicle` (which only works with gif files), and `imagemagick` (which chokes on large files).  

`ffmpeg` to the rescue? try it out!  
WARNING: gif sizes seem to be huge...
https://askubuntu.com/questions/648603/how-to-create-an-animated-gif-from-mp4-video-via-command-line  
```
sudo apt-get install ffmpeg
wget -O opengl-rotating-triangle.mp4 https://github.com/cirosantilli/media/blob/master/opengl-rotating-triangle.mp4?raw=true
ulimit -Sv 1000000
ffmpeg \
  -i triangle.mp4 \
  -r 15 \
  -vf scale=512:-1 \
  -ss 00:00:03 -to 00:00:06 \
  triangle.gif
```
