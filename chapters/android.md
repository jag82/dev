# android


## android studio shorcuts

cmd+1 			project sidebar
cmd+shift+O 	open file
cmd+alt+L 		format code


ctrl+V 			version control menu
cmd+shift+K 	push + commit
cmd+K 			commit


cmd+/			fit to screen
ctrl+shift l/r 	cycle between code/split/design